﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using DRM.Webportal.DAL;

namespace DRM.Webportal.Infrastructure
{
    public class IocContainerConfig
    {
        public static void RegisterContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(typeof(MvcApplication).Assembly);
            //builder.RegisterType<EmployeeRepository>().As<IEmployeeRepository>();
            //builder.RegisterType<Entities>().As<IContext>();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}