﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DRM.Webportal.DAL;
using DRM.Webportal.Models;

namespace DRM.Webportal.Infrastructure
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => cfg.AddProfile(new PersonProfile()));
        }
    }

    public class PersonProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.Initialize(cfg => cfg.ConstructServicesUsing(t => DependencyResolver.Current.GetService(t)));
        }
    }
}