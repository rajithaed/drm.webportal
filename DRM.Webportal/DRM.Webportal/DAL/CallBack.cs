//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DRM.Webportal.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CallBack
    {
        [DisplayName("ID")]
        public int Id { get; set; }

        [DisplayName("Cust Id")]
        public int custId { get; set; }

        [Required]
        [DisplayName("Callback Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> date { get; set; }

        [Required]
        [DisplayName("Callback Time")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public Nullable<System.TimeSpan> time { get; set; }

        [DisplayName("Notes")]
        [DataType(DataType.MultilineText)]
        public string notes { get; set; }

        [DisplayName("Created Agent")]
        public string agent { get; set; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        public Nullable<System.DateTime> datetime { get; set; }

        public string status { get; set; }

        public string company { get; set; }
    }
}
