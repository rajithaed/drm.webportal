﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DRM.Webportal.Infrastructure;

namespace DRM.Webportal
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            //IocContainerConfig.RegisterContainer();
            AutoMapperConfig.Configure();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}