﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DRM.Webportal.Startup))]
namespace DRM.Webportal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
