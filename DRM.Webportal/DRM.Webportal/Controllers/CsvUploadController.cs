﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DRM.Webportal.DAL;

namespace DRM.Webportal.Controllers
{
    public class CsvUploadController : Controller
    {

        public object UserAccess;

        public CsvUploadController()
        {
            UserAccess = System.Web.HttpContext.Current.Session["LoggedInUserAccess"];
        }

        public ActionResult Index()
        {
            if ((string) UserAccess == "admin" || (string) UserAccess == "user")
            {
                return View();
            }
            TempData["notice"] = "You do not have permission to access this page. Login using correct credentials !";

            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            try
            {
                if (Request.Files["file"].ContentLength > 0)
                {
                    string fileExtension = Path.GetExtension(Request.Files["file"].FileName);

                    if (fileExtension == ".csv" || fileExtension == ".CSV")
                    {
                        string fileLocation = Server.MapPath("~/ExcelUploadFiles/") + Request.Files["file"].FileName;
                        if (System.IO.File.Exists(fileLocation))
                        {
                            System.IO.File.Delete(fileLocation);
                        }
                        Request.Files["file"].SaveAs(fileLocation);


                        //string[] lines = File.ReadAllLines(file);

                        string[] lines = System.IO.File.ReadAllLines(fileLocation);

                        //Remove Header line
                        lines = lines.Skip(1).ToArray();
                        List<PPI> emList = lines.Select(line => line.Split(new[] {','})).Select(fields => new PPI
                        {
                            CustomerId = fields[0].Replace("\"", ""), // removed "" 
                            Title = fields[1].Replace("\"", ""),
                            FirstName = fields[2].Replace("\"", ""),
                            LastName = fields[3].Replace("\"", ""),
                            Address = fields[4].Replace("\"", ""),
                            Postcode = fields[5].Replace("\"", ""),
                            Dob = fields[6].Replace("\"", ""),
                            HomePhone = fields[7].Replace("\"", ""),
                            MobilePhone = fields[8].Replace("\"", ""),
                            WorkPhone = fields[9].Replace("\"", ""),
                            Email = fields[10].Replace("\"", ""),
                            FinanceType = fields[11].Replace("\"", ""),
                            Lender = fields[12].Replace("\"", ""),
                            Amount = fields[13].Replace("\"", ""),
                            StartDate = fields[14].Replace("\"", ""),
                            EndDate = fields[15].Replace("\"", ""),
                            Arrears = fields[16].Replace("\"", "")
                        }).ToList();

                        // Update database data
                        TestDBEntities entities;
                        using (entities = new TestDBEntities())
                        {
                            foreach (PPI i in emList)
                            {
                                PPI v = entities.PPIs.FirstOrDefault(a => a.Id.Equals(i.Id));
                                if (v != null)
                                {
                                    v.CustomerId = i.CustomerId;
                                    v.Title = i.Title;
                                    v.FirstName = i.FirstName;
                                    v.LastName = i.LastName;
                                    v.Address = i.Address;
                                    v.Postcode = i.Postcode;
                                    v.Dob = i.Dob;
                                    v.HomePhone = i.HomePhone;
                                    v.MobilePhone = i.MobilePhone;
                                    v.WorkPhone = i.WorkPhone;
                                    v.Email = i.Email;
                                    v.FinanceType = i.FinanceType;
                                    v.Lender = i.Lender;
                                    v.Amount = i.Amount;
                                    v.StartDate = i.StartDate;
                                    v.EndDate = i.EndDate;
                                    v.Arrears = i.Arrears;
                                }
                                else
                                {
                                    entities.PPIs.Add(i);
                                }
                            }

                            entities.SaveChanges();


                            TempData["notice"] = "Successfully Uploaded.";
                        }
                    }
                }
                return View();
            }
            catch
                (Exception
                    ex)
            {
                TempData["notice"] = "ERROR : " + ex.Message;
                return View();
            }
        }

        public FileResult Download(string id)
        {
            string filename = Server.MapPath("~/Content/drmDataUploadTemplate.csv");
            const string contentType = "text/csv";
            return File(filename, contentType, "drmDataUploadTemplate.csv");
        }
    }
}