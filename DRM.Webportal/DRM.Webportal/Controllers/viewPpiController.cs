﻿using System;
using System.Linq;
using System.Web.Mvc;
using DRM.Webportal.DAL;

namespace DRM.Webportal.Controllers
{
    public class ViewPpiController : Controller
    {
        public DateTime DateTimeStamp;
        public object UserAccess;
        public object UserId;

        public ViewPpiController()
        {
            UserId = System.Web.HttpContext.Current.Session["LoggedInUserId"];
            UserAccess = System.Web.HttpContext.Current.Session["LoggedInUserAccess"];
            DateTimeStamp = DateTime.Now;
        }

        public ActionResult Index(int? ppiId)
        {
            try
            {
                var entities = new TestDBEntities();

                if ((string) UserAccess == "admin" || (string) UserAccess == "user")
                {
                    PPI selectedPpiRecord = entities.PPIs.FirstOrDefault(a => a.Id == ppiId);

                    return View(selectedPpiRecord);
                }
                TempData["notice"] = "You do not have permission to access this page. Login using correct credentials !";
                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
                return RedirectToAction("Index", "ViewPpi");
            }
        }

        [HttpPost]
        public ActionResult EndClaim(PPI ppi)
        {
            var entities = new TestDBEntities();

            if ((string) UserAccess == "admin" || (string) UserAccess == "user")
            {
                UpdateCompletedRecord(ppi, entities);

                return RedirectToAction("Index", "Ppi");
            }
            return RedirectToAction("Login", "Account");
        }

        private void UpdateCompletedRecord(PPI ppi, TestDBEntities entities)
        {
            PPI selectedPpiRecord = entities.PPIs.FirstOrDefault(a => a.Id == ppi.Id);
            if (selectedPpiRecord != null)
            {
                selectedPpiRecord.AgentStatus = "Completed";
                selectedPpiRecord.LastUpdatedDate = DateTimeStamp;
                selectedPpiRecord.LastUpdatedUser = (int?) UserId;
            }

            entities.SaveChanges();
        }
    }
}