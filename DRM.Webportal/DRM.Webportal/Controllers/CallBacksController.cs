﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using DRM.Webportal.DAL;

namespace DRM.Webportal.Controllers
{
    public class CallBackViewModel
    {
        public CallBackStatistics CallBackStatistics { get; set; }
        public IEnumerable<CallBack> CallBacks { get; set; }

    }

    public class CallBackStatistics
    {
        public int TotalCallBacks { get; set; }
        public int Attened { get; set; }
        public int UnAttended { get; set; }
    }

    public class CallBacksController : Controller
    {
        private readonly TestDBEntities db = new TestDBEntities();
        public DateTime DateTimeStamp;
        public object UserAccess;
        public object UserId;
        public object UserName;
        private object UserCompany;

        public CallBacksController()
        {
            UserName = System.Web.HttpContext.Current.Session["LoggedInUserName"];
            UserId = System.Web.HttpContext.Current.Session["LoggedInUserId"];
            UserAccess = System.Web.HttpContext.Current.Session["LoggedInUserAccess"];
            UserCompany = System.Web.HttpContext.Current.Session["LoggedInUserCompany"];
            DateTimeStamp = DateTime.Now;
        }

        // GET: CallBacks
        public ActionResult Index(string selectedDate)
        {
            try
            {
                if ((string)UserAccess == "admin" || (string)UserAccess == "user" || (string)UserAccess == "external")
                { 
                    var viewModel = new CallBackViewModel();
                    if (selectedDate != null)
                    {  
                       
                        using (var entities = new TestDBEntities())
                        {
                            DateTime date = Convert.ToDateTime(selectedDate);
                            var selectedDateCallBacks = new List<CallBack>(
                                entities.CallBacks.Where(
                                    x =>
                                        x.date.Value.Year == date.Year && x.date.Value.Month == date.Month &&
                                        x.date.Value.Day == date.Day && x.company == (string)UserCompany));

                            TempData["notice"] = "Callbacks for " + date.ToShortDateString() + " are selected below !";

                            //===
                            viewModel.CallBacks = selectedDateCallBacks;
                            
                            //get ppi record agent status to check callback status
                            int attendedCount = 0;
                            int unAttendedCount = 0;
                            int totalCallBacks = selectedDateCallBacks.Count;

                            foreach (var item in selectedDateCallBacks)
                            {
                                var ppi = entities.PPIs.FirstOrDefault(x => x.Id == item.custId);
                                if (ppi != null)
                                {
                                    var status = ppi.AgentStatus;
                                    
                                    if (status == "callback")
                                    {
                                        unAttendedCount = unAttendedCount + 1;
                                    }
                                }
                            }

                            var selectedStats = new CallBackStatistics();
                            selectedStats.TotalCallBacks = totalCallBacks;
                            selectedStats.UnAttended = unAttendedCount;
                            selectedStats.Attened = totalCallBacks - unAttendedCount;

                            viewModel.CallBackStatistics = selectedStats;
                            //====

                            return View(viewModel);
                        }
                    }
                    using (var entities = new TestDBEntities())
                    {
                        DateTime date = Convert.ToDateTime(DateTime.Now);
                        var todayCallBacks = new List<CallBack>(
                            entities.CallBacks.Where(
                                x =>
                                    x.date.Value.Year == date.Year && x.date.Value.Month == date.Month &&
                                    x.date.Value.Day == date.Day && x.company == (string) UserCompany));

                        TempData["notice"] = "Today's Callbacks are listed below !";

                        //
                        viewModel.CallBacks = todayCallBacks;
                        viewModel.CallBackStatistics = new CallBackStatistics();
                        //

                        //get ppi record agent status to check callback status
                        int attendedCount = 0;
                        int unAttendedCount = 0;
                        int totalCallBacks = todayCallBacks.Count;

                        foreach (var item in todayCallBacks)
                        {
                            var ppi = entities.PPIs.FirstOrDefault(x => x.Id == item.custId);
                            if (ppi != null)
                            {
                                var status = ppi.AgentStatus;
                                if (status == "callback")
                                {
                                    unAttendedCount = unAttendedCount + 1;
                                }
                            }
                        }

                        var selectedStats = new CallBackStatistics();
                        selectedStats.TotalCallBacks = totalCallBacks;
                        selectedStats.UnAttended = unAttendedCount;
                        selectedStats.Attened = totalCallBacks - unAttendedCount;

                        viewModel.CallBackStatistics = selectedStats;
                        //====

                        return View(viewModel);
                    }
                }
                TempData["notice"] =
                    "You do not have permission to access this page. Please Login using correct credentials !";

                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
                return RedirectToAction("Index", "Ppi");
            }
        }

        [HttpPost]
        public ActionResult Update(string selectedDate)
        {
            try
            {
                List<CallBack> callBacksList;
                using (var entities = new TestDBEntities())
                {
                    DateTime date = Convert.ToDateTime(selectedDate);
                    callBacksList =
                        new List<CallBack>(
                            entities.CallBacks.Where(
                                x =>
                                    x.date.Value.Year == date.Year && x.date.Value.Month == date.Month &&
                                    x.date.Value.Day == date.Day));
                }


                return RedirectToAction("Index", "CallBacks", callBacksList);
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
                return RedirectToAction("Index", "CallBacks");
            }
        }

        // GET: CallBacks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CallBack callBack = await db.CallBacks.FindAsync(id);
            if (callBack == null)
            {
                return HttpNotFound();
            }
            return View(callBack);
        }

        // GET: CallBacks/Create
        public ActionResult Create(int custId, string custName)
        {
            var callBackRecord = new CallBack {custId = custId};
            TempData["notice"] = "Customer Name = " + custName;
            return View(callBackRecord);
        }

        // POST: CallBacks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(Include = "Id,custId,date,time,notes,agent,datetime,status,company")] CallBack callBack)
        {
            if (ModelState.IsValid)
            {
                DateTime date = Convert.ToDateTime(callBack.date);
                callBack.date = date;
                callBack.agent = UserName.ToString();
                callBack.datetime = DateTime.Now;
                callBack.company = UserCompany.ToString();
                db.CallBacks.Add(callBack);
                await db.SaveChangesAsync();

                TestDBEntities entities;
                PPI callBackRecord;
                UpdatePpiRecordWithCallBackConfirmation(callBack, out entities, out callBackRecord);

                return RedirectToAction("Index");
            }

            return View(callBack);
        }

        private void UpdatePpiRecordWithCallBackConfirmation(CallBack callBack, out TestDBEntities entities,
            out PPI callBackRecord)
        {
            entities = new TestDBEntities();
            callBackRecord = entities.PPIs.FirstOrDefault(x => x.Id == callBack.custId);

            if (callBackRecord != null)
            {
                callBackRecord.AgentStatus = "callback";
                callBackRecord.Allocated = "yes";
                callBackRecord.LastUpdatedDate = DateTimeStamp;
                callBackRecord.LastUpdatedUser = (int?) UserId;
            }
            entities.SaveChanges();
        }

        // GET: CallBacks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CallBack callBack = await db.CallBacks.FindAsync(id);
            if (callBack == null)
            {
                return HttpNotFound();
            }
            return View(callBack);
        }

        // POST: CallBacks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(Include = "Id,custId,date,time,notes,agent,datetime")] CallBack callBack)
        {
            if (ModelState.IsValid)
            {
                db.Entry(callBack).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(callBack);
        }

        // GET: CallBacks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CallBack callBack = await db.CallBacks.FindAsync(id);
            if (callBack == null)
            {
                return HttpNotFound();
            }
            return View(callBack);
        }

        // POST: CallBacks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            CallBack callBack = await db.CallBacks.FindAsync(id);
            db.CallBacks.Remove(callBack);
            await db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}