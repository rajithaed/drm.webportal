﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DRM.Webportal.DAL;

namespace DRM.Webportal.Controllers
{
    public class ReportsController : Controller
    {
        // 

        public object UserAccess;

        public ReportsController()
        {
            UserAccess = System.Web.HttpContext.Current.Session["LoggedInUserAccess"];
        }

        public ActionResult Index()
        {
            if ((string) UserAccess == "admin")
            {
                return View();
            }
            TempData["notice"] =
                "You do not have permission to access this page. Please Login using correct credentials !";

            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public void Download()
        {
            List<PPI> ppiList;
            using (var entities = new TestDBEntities())
            {
                ppiList = entities.PPIs.ToList();
            }

            if (ppiList.Count > 0)
            {
                StringBuilder sb = GenerateFullReport(ppiList);

                // Download Here

                HttpContext context = HttpContext.ApplicationInstance.Context;
                context.Response.Write(sb.ToString());
                context.Response.ContentType = "text/csv";
                context.Response.AddHeader("Content-Disposition", "attachment; filename=PpiStatusReportAll.csv");
                context.Response.End();
            }
            else
            {
                TempData["notice"] = "Data not Found!";
            }
        }

        private static StringBuilder GenerateFullReport(IEnumerable<PPI> ppiList)
        {
            const string header =
                @"""ID"",""Customer Id"",""Reference"",""First tName"",""Last Name"",""Address"",""Postcode"",""Date of Birth"",""Mobile Phone"",""Email"",""Has PPI"",""Finance Type"",""Lender"",""Amount"",""Submit To Company Id"",""Agent Status"",""External Status"",""LastUpdated Date"",""LastUpdated User"",""Allocated Date"",""Processed Date"",""Agent Reject Reason"",""External Reject Reason"",""External Notes"",""Allocated to"",""Processed by""";
            var sb = new StringBuilder();
            sb.AppendLine(header);

            foreach (PPI i in ppiList)
            {
                sb.AppendLine(string.Join(",",
                    string.Format(@"""{0}""", i.Id),
                    string.Format(@"""{0}""", i.CustomerId),
                    string.Format(@"""{0}""", i.Reference),
                    string.Format(@"""{0}""", i.FirstName),
                    string.Format(@"""{0}""", i.LastName),
                    string.Format(@"""{0}""", i.Address),
                    string.Format(@"""{0}""", i.Postcode),
                    string.Format(@"""{0}""", i.Dob),
                    string.Format(@"""{0}""", i.MobilePhone),
                    string.Format(@"""{0}""", i.Email),
                    string.Format(@"""{0}""", i.HasPPI),
                    string.Format(@"""{0}""", i.FinanceType),
                    string.Format(@"""{0}""", i.Lender),
                    string.Format(@"""{0}""", i.Amount),
                    string.Format(@"""{0}""", i.SubmitToCompanyId),
                    string.Format(@"""{0}""", i.AgentStatus),
                    string.Format(@"""{0}""", i.ExternalStatus),
                    string.Format(@"""{0}""", i.LastUpdatedDate),
                    string.Format(@"""{0}""", i.LastUpdatedUser),
                    string.Format(@"""{0}""", i.AllocatedDate),
                    string.Format(@"""{0}""", i.ProcessedDate),
                    string.Format(@"""{0}""", i.AgentRejectReason),
                    string.Format(@"""{0}""", i.ExternalRejectReason),
                    string.Format(@"""{0}""", i.ExternalNotes),
                    string.Format(@"""{0}""", i.AllocatedTo),
                    string.Format(@"""{0}""", i.ProcessedBy)));
            }
            return sb;
        }

        [HttpPost]
        public ActionResult DownloadDailyReport(string selectedDate)
        {
            try
            {
                if (selectedDate != "")
                {
                    List<PPI> ppiList;
                    using (var entities = new TestDBEntities())
                    {
                        DateTime date = Convert.ToDateTime(selectedDate);
                        ppiList =
                            new List<PPI>(
                                entities.PPIs.Where(
                                    x =>
                                        x.AllocatedDate.Value.Year == date.Year &&
                                        x.AllocatedDate.Value.Month == date.Month &&
                                        x.AllocatedDate.Value.Day == date.Day));
                    }

                    if (ppiList.Count > 0)
                    {
                        StringBuilder sb = GenerateDailyReport(ppiList);

                        // Download Here

                        HttpContext context = HttpContext.ApplicationInstance.Context;
                        context.Response.Write(sb.ToString());
                        context.Response.ContentType = "text/csv";
                        context.Response.AddHeader("Content-Disposition", "attachment; filename=PpiStatusReportAll.csv");
                        context.Response.End();
                    }
                    else
                    {
                        TempData["notice"] = "Data not Found!";
                    }
                }
                else
                {
                    TempData["notice"] = "Please select a date to generate the report";
                }
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
            }


            return RedirectToAction("Index", "Reports");
        }

        private static StringBuilder GenerateDailyReport(List<PPI> ppiList)
        {
            const string header =
                @"""ID"",""Customer Id"",""Reference"",""First tName"",""Last Name"",""Address"",""Postcode"",""Date of Birth"",""Mobile Phone"",""Email"",""Has PPI"",""Finance Type"",""Lender"",""Amount"",""Submit To Company Id"",""Agent Status"",""External Status"",""LastUpdated Date"",""LastUpdated User"",""Allocated Date"",""Processed Date"",""Agent Reject Reason"",""External Reject Reason"",""External Notes"",""Allocated to"",""Processed by""";
            var sb = new StringBuilder();
            sb.AppendLine(header);

            foreach (PPI i in ppiList)
            {
                sb.AppendLine(string.Join(",",
                    string.Format(@"""{0}""", i.Id),
                    string.Format(@"""{0}""", i.CustomerId),
                    string.Format(@"""{0}""", i.Reference),
                    string.Format(@"""{0}""", i.FirstName),
                    string.Format(@"""{0}""", i.LastName),
                    string.Format(@"""{0}""", i.Address),
                    string.Format(@"""{0}""", i.Postcode),
                    string.Format(@"""{0}""", i.Dob),
                    string.Format(@"""{0}""", i.MobilePhone),
                    string.Format(@"""{0}""", i.Email),
                    string.Format(@"""{0}""", i.HasPPI),
                    string.Format(@"""{0}""", i.FinanceType),
                    string.Format(@"""{0}""", i.Lender),
                    string.Format(@"""{0}""", i.Amount),
                    string.Format(@"""{0}""", i.SubmitToCompanyId),
                    string.Format(@"""{0}""", i.AgentStatus),
                    string.Format(@"""{0}""", i.ExternalStatus),
                    string.Format(@"""{0}""", i.LastUpdatedDate),
                    string.Format(@"""{0}""", i.LastUpdatedUser),
                    string.Format(@"""{0}""", i.AllocatedDate),
                    string.Format(@"""{0}""", i.ProcessedDate),
                    string.Format(@"""{0}""", i.AgentRejectReason),
                    string.Format(@"""{0}""", i.ExternalRejectReason),
                    string.Format(@"""{0}""", i.ExternalNotes),
                    string.Format(@"""{0}""", i.AllocatedTo),
                    string.Format(@"""{0}""", i.ProcessedBy)));
            }
            return sb;
        }
    }
}