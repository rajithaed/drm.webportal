﻿using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using DRM.Webportal.DAL;

namespace DRM.Webportal.Controllers
{
    public class UsersController : Controller
    {
        private readonly TestDBEntities db = new TestDBEntities();

        public object UserAccess;

        public UsersController()
        {
            UserAccess = System.Web.HttpContext.Current.Session["LoggedInUserAccess"];
        }

        public async Task<ActionResult> Index()
        {
            if ((string) UserAccess == "admin")
            {
                return View(await db.Users.ToListAsync());
            }
            TempData["notice"] =
                "You do not have permission to access this page. Please Login using correct credentials !";

            return RedirectToAction("Login", "Account");
        }

        // GET: Users/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            // object userAccess = Session["LoggedInUserAccess"];

            if ((string) UserAccess == "admin")
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                User user = await db.Users.FindAsync(id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                return View(user);
            }
            TempData["notice"] =
                "You do not have permission to access this page. Please Login using correct credentials !";

            return RedirectToAction("Login", "Account");
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            if ((string) UserAccess == "admin")
            {
                return View();
            }
            TempData["notice"] =
                "You do not have permission to access this page. Please Login using correct credentials !";

            return RedirectToAction("Login", "Account");
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(
                Include = "ID,user_name,password,access_level,company,Email,RememberMe,confirmPassword,createdDateTime")
            ] User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(
                Include = "ID,user_name,password,access_level,company,Email,RememberMe,confirmPassword,createdDateTime")
            ] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = (EntityState) System.Data.EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            User user = await db.Users.FindAsync(id);
            db.Users.Remove(user);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}