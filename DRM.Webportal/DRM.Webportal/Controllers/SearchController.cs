﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using DRM.Webportal.DAL;

namespace DRM.Webportal.Controllers
{
    public class SearchController : Controller
    {
        private readonly TestDBEntities _db = new TestDBEntities();
        public DateTime DateTimeStamp;
        public object UserAccess;
        public object UserId;
        public object UserName;

        public SearchController()
        {
            UserName = System.Web.HttpContext.Current.Session["LoggedInUserName"];
            UserId = System.Web.HttpContext.Current.Session["LoggedInUserId"];
            UserAccess = System.Web.HttpContext.Current.Session["LoggedInUserAccess"];
            DateTimeStamp = DateTime.Now;
        }

        public ActionResult Index(string searchString, string searchCategory)
        {
            try
            {
                if ((string) UserAccess == "admin" || (string) UserAccess == "user")
                {
                    if (!string.IsNullOrEmpty(searchCategory))
                    {
                        if (!String.IsNullOrEmpty(searchString))
                        {
                            if (searchCategory == "Postcode")
                            {
                                IQueryable<PPI> ppis = _db.PPIs.Where(x => x.Postcode.Contains(searchString));
                                return View(ppis.ToList());
                            }
                            if (searchCategory == "MobileNo")
                            {
                                IQueryable<PPI> ppis = _db.PPIs.Where(x => x.MobilePhone.Contains(searchString));
                                return View(ppis.ToList());
                            }
                            if (searchCategory == "FirstName")
                            {
                                IQueryable<PPI> ppis = _db.PPIs.Where(x => x.FirstName.Contains(searchString));
                                return View(ppis.ToList());
                            }
                            if (searchCategory == "LastName")
                            {
                                IQueryable<PPI> ppis = _db.PPIs.Where(x => x.LastName.Contains(searchString));
                                return View(ppis.ToList());
                            }
                            if (searchCategory == "Reference")
                            {
                                IQueryable<PPI> ppis = _db.PPIs.Where(x => x.Reference.Contains(searchString));
                                return View(ppis.ToList());
                            }
                            return View(_db.PPIs.Take(0).ToList());
                        }
                        TempData["notice"] = "Enter a search text";
                        return View(_db.PPIs.Take(0).ToList());
                    }
                    TempData["notice"] = "Please Selecte a Search Criteria ";
                    return View(_db.PPIs.Take(0).ToList());
                }
                TempData["notice"] = "You do not have permission to access this page. Login using correct credentials !";
                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
                return RedirectToAction("Index", "Search");
            }
        }


        // GET: Search/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PPI pPi = await _db.PPIs.FindAsync(id);
            if (pPi == null)
            {
                return HttpNotFound();
            }
            return View(pPi);
        }

        [HttpPost]
        public RedirectToRouteResult SubmitSearchRecord(PPI model)
        {
            try
            {
                var entities = new TestDBEntities();

                if (Request.Form["submitbutton"] != null)
                {
                    // submit to process
                    PPI recordToBeUpdated = entities.PPIs.FirstOrDefault(x => x.Id == model.Id);

                    if (recordToBeUpdated != null)
                    {
                        UpdateSubmittedRecord(model, recordToBeUpdated, entities);

                        TempData["notice"] = "Customer Details Submitted to Process !";

                        return RedirectToAction("Index", "ViewPpi", new {ppiId = model.Id});
                    }
                    TempData["notice"] = "No Record Id. Something has gone wrong !!!";
                    return RedirectToAction("Details", "Search");
                }

                if (Request.Form["rejectbutton"] != null)
                {
                    // Reject the record and save.
                    PPI recordToBeUpdated = entities.PPIs.FirstOrDefault(x => x.Id == model.Id);

                    if (recordToBeUpdated != null)
                    {
                        UpdateRejectedRecord(model, recordToBeUpdated, entities);

                        TempData["notice"] = "Customer Details Updated !";

                        return RedirectToAction("Index", "Search");
                    }
                    TempData["notice"] = "No Record Id. Something has gone wrong !!!";
                    return RedirectToAction("Details", "Search");
                }

                return RedirectToAction("Details", "Search");
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;

                return RedirectToAction("Details", "Search");
            }
        }

        private void UpdateRejectedRecord(PPI model, PPI recordToBeUpdated, TestDBEntities entities)
        {
            recordToBeUpdated.FirstName = model.FirstName;
            recordToBeUpdated.LastName = model.LastName;
            recordToBeUpdated.AccessToAccountNumber = model.AccessToAccountNumber;
            recordToBeUpdated.AccessToDocuments = model.AccessToDocuments;
            recordToBeUpdated.Address = model.Address;
            recordToBeUpdated.Amount = model.Amount;
            recordToBeUpdated.Arrears = model.Arrears;
            recordToBeUpdated.ClaimedPPIBefore = model.ClaimedPPIBefore;
            recordToBeUpdated.ContactLender = model.ContactLender;
            recordToBeUpdated.CustomerId = model.CustomerId;
            recordToBeUpdated.Dob = model.Dob;
            recordToBeUpdated.Email = model.Email;
            recordToBeUpdated.EndDate = model.EndDate;
            recordToBeUpdated.FinanceType = model.FinanceType;
            recordToBeUpdated.HasPPI = model.HasPPI;
            recordToBeUpdated.HomePhone = model.HomePhone;
            recordToBeUpdated.Lender = model.Lender;
            recordToBeUpdated.MobilePhone = model.MobilePhone;
            recordToBeUpdated.Postcode = model.Postcode;
            recordToBeUpdated.StartDate = model.StartDate;
            recordToBeUpdated.SubmitToCompanyId = model.SubmitToCompanyId;
            recordToBeUpdated.Title = model.Title;
            recordToBeUpdated.WorkPhone = model.WorkPhone;
            recordToBeUpdated.AgentNotes = model.AgentNotes;
            recordToBeUpdated.ExternalNotes = model.ExternalNotes;

            recordToBeUpdated.AgentStatus = "Rejected";
            recordToBeUpdated.AgentRejectReason = model.AgentRejectReason;

            recordToBeUpdated.LastUpdatedDate = DateTimeStamp;
            recordToBeUpdated.LastUpdatedUser = (int?) UserId;


            entities.SaveChanges();
        }

        private void UpdateSubmittedRecord(PPI model, PPI recordToBeUpdated, TestDBEntities entities)
        {
            recordToBeUpdated.FirstName = model.FirstName;
            recordToBeUpdated.LastName = model.LastName;
            recordToBeUpdated.AccessToAccountNumber = model.AccessToAccountNumber;
            recordToBeUpdated.AccessToDocuments = model.AccessToDocuments;
            recordToBeUpdated.Address = model.Address;
            recordToBeUpdated.Amount = model.Amount;
            recordToBeUpdated.Arrears = model.Arrears;
            recordToBeUpdated.ClaimedPPIBefore = model.ClaimedPPIBefore;
            recordToBeUpdated.ContactLender = model.ContactLender;
            recordToBeUpdated.CustomerId = model.CustomerId;
            recordToBeUpdated.Dob = model.Dob;
            recordToBeUpdated.Email = model.Email;
            recordToBeUpdated.EndDate = model.EndDate;
            recordToBeUpdated.FinanceType = model.FinanceType;
            recordToBeUpdated.HasPPI = model.HasPPI;
            recordToBeUpdated.HomePhone = model.HomePhone;
            recordToBeUpdated.Lender = model.Lender;
            recordToBeUpdated.MobilePhone = model.MobilePhone;
            recordToBeUpdated.Postcode = model.Postcode;
            recordToBeUpdated.StartDate = model.StartDate;
            recordToBeUpdated.SubmitToCompanyId = model.SubmitToCompanyId;
            recordToBeUpdated.Title = model.Title;
            recordToBeUpdated.WorkPhone = model.WorkPhone;
            recordToBeUpdated.AgentNotes = model.AgentNotes;
            recordToBeUpdated.ExternalNotes = model.ExternalNotes;
            recordToBeUpdated.AgentRejectReason = model.AgentRejectReason;


            recordToBeUpdated.Reference = GenerateUniqueReference();
            recordToBeUpdated.AgentStatus = "Submitted";

            recordToBeUpdated.LastUpdatedDate = DateTimeStamp;
            recordToBeUpdated.LastUpdatedUser = (int?) UserId;

            recordToBeUpdated.AllocatedDate = DateTimeStamp; // this is only when submitted
            recordToBeUpdated.AllocatedTo = (string) UserName; // this is only when submitted


            entities.SaveChanges();
        }

        public string GenerateUniqueReference()
        {
            //alpha numeric
            //string def = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            //var rnd = new Random();
            //var ret = new StringBuilder();
            //for (int i = 0; i < 6; i++)
            //    ret.Append(def.Substring(rnd.Next(def.Length), 1));
            //return ret.ToString();

            // numeric
            var rnd = new Random();
            int myRandomNo = rnd.Next(0000001, 99999999);

            return myRandomNo.ToString(CultureInfo.InvariantCulture);
        }


        // GET: Search/Create
        public ActionResult Create()
        {
            try
            {
                if ((string) UserAccess == "admin" || (string) UserAccess == "user")
                {
                    return View();
                }
                TempData["notice"] = "You do not have permission to access this page. Login using correct credentials !";
                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
                return RedirectToAction("Create", "Search");
            }
        }

        // POST: Search/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(
                Include =
                    "Id,CustomerId,HasPPI,ClaimedPPIBefore,FinanceType,Lender,Amount,StartDate,EndDate,Arrears,AccessToDocuments,ContactLender,AccessToAccountNumber,SubmitToCompanyId,AgentStatus,CreatedDate,LastUpdatedDate,LastUpdatedUser,Title,FirstName,LastName,Address,Postcode,Dob,HomePhone,MobilePhone,WorkPhone,Email,Allocated,AllocatedTo,Processed,ProcessedBy,Reference,AllocatedDate,ProcessedDate,AgentRejectReason,AgentNotes,ExternalRejectReason,ExternalNotes,ExternalStatus,uploadDate"
                )] PPI pPI)
        {
            if (ModelState.IsValid)
            {
                _db.PPIs.Add(pPI);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(pPI);
        }

        // GET: Search/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            try
            {
                if ((string) UserAccess == "admin" || (string) UserAccess == "user")
                {
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    PPI pPI = await _db.PPIs.FindAsync(id);
                    if (pPI == null)
                    {
                        return HttpNotFound();
                    }
                    return View(pPI);
                }
                TempData["notice"] = "You do not have permission to access this page. Login using correct credentials !";
                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
                return RedirectToAction("Create", "Search");
            }
        }

        // POST: Search/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(
                Include =
                    "Id,CustomerId,HasPPI,ClaimedPPIBefore,FinanceType,Lender,Amount,StartDate,EndDate,Arrears,AccessToDocuments,ContactLender,AccessToAccountNumber,SubmitToCompanyId,AgentStatus,CreatedDate,LastUpdatedDate,LastUpdatedUser,Title,FirstName,LastName,Address,Postcode,Dob,HomePhone,MobilePhone,WorkPhone,Email,Allocated,AllocatedTo,Processed,ProcessedBy,Reference,AllocatedDate,ProcessedDate,AgentRejectReason,AgentNotes,ExternalRejectReason,ExternalNotes,ExternalStatus,uploadDate"
                )] PPI pPI)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(pPI).State = (EntityState) System.Data.EntityState.Modified;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(pPI);
        }

        // GET: Search/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PPI pPI = await _db.PPIs.FindAsync(id);
            if (pPI == null)
            {
                return HttpNotFound();
            }
            return View(pPI);
        }

        // POST: Search/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            PPI pPI = await _db.PPIs.FindAsync(id);
            _db.PPIs.Remove(pPI);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}