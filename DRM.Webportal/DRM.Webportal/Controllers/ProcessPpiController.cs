﻿using System;
using System.Linq;
using System.Web.Mvc;
using DRM.Webportal.DAL;

namespace DRM.Webportal.Controllers
{
    public class ProcessPpiController : Controller
    {
        public DateTime DateTimeStamp;
        public object UserAccess;
        public object UserId;
        public object UserName;

        public ProcessPpiController()
        {
            UserAccess = System.Web.HttpContext.Current.Session["LoggedInUserAccess"];
            UserId = System.Web.HttpContext.Current.Session["LoggedInUserId"];
            UserName = System.Web.HttpContext.Current.Session["LoggedInUserName"];
            DateTimeStamp = DateTime.Now;
        }

        // GET: ProcessPpi
        public ActionResult Index(PPI ppi)
        {
            if ((string) UserAccess == "admin" || (string) UserAccess == "external")
            {
                return View(ppi);
            }
            TempData["notice"] = "You do not have permission to access this page. Login using correct credentials !";
            return RedirectToAction("Login", "Account");
        }

        public ActionResult ShowCallbackRecord(int? id)
        {
            if ((string)UserAccess == "admin" || (string)UserAccess == "external")
            {
                var entities = new TestDBEntities();
                PPI personRecord = entities.PPIs.FirstOrDefault(x => x.Id == id);
                return RedirectToAction("Index", "ProcessPpi", personRecord);
            }
            TempData["notice"] = "You do not have permission to access this page. Login using correct credentials !";
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public RedirectToRouteResult LoadrecordToProcess(PPI model, string searchString)
        {
            try
            {
                var entities = new TestDBEntities();

                if (Request.Form["searchRecord"] != null)
                {
                    ////get record from PPI table based on reference
                    PPI personRecord = entities.PPIs.FirstOrDefault(x => x.Reference == searchString);

                    if (personRecord != null)
                    {
                        // add the user who process the record into DB
                        AddUserDetailsWhoProcessTheRecord(personRecord, entities);

                        TempData["notice"] = "Record Found !";
                    }
                    else
                    {
                        TempData["notice"] = "Record Does not Exist !!!";
                    }

                    return RedirectToAction("Index", "ProcessPpi", personRecord);
                }

                if (Request.Form["submitRecord"] != null)
                {
                    // update and save changes records

                    PPI recordToBeUpdated = entities.PPIs.FirstOrDefault(x => x.Id == model.Id);
                    if (recordToBeUpdated != null)
                    {
                        SaveAcceptedRecord(model, recordToBeUpdated, DateTimeStamp, UserName, entities);
                    }
                    
                    TempData["notice"] = "Customer Processing completed !";

                    return RedirectToAction("Index", "ProcessPpi");
                }
                if (Request.Form["rejectRecord"] != null)
                {
                    // Reject the record and save.

                    PPI recordToBeUpdated = entities.PPIs.FirstOrDefault(x => x.Id == model.Id);

                    if (recordToBeUpdated != null)
                    {
                        SaveRejectedRecord(model, recordToBeUpdated, UserName, DateTimeStamp, UserId, entities);
                    }

                    TempData["notice"] = "Customer Details Updated !";

                    return RedirectToAction("Index", "ProcessPpi");
                }

                return RedirectToAction("Index", "ProcessPpi");
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
                return RedirectToAction("Index", "ProcessPpi");
            }
        }

        public ActionResult CallBack(int customerId)
        {
            //Session["NextRecordButtonStatus"] = "on";

            // set record as a callback
            PPI callBackRecord;
            var entities = SaveRecordAsCallBack(customerId, out callBackRecord);

            var ppiRec = entities.PPIs.FirstOrDefault(x => x.Id == customerId);
            string custFullName = null;
            if (ppiRec != null)
            {
                custFullName = ppiRec.FirstName + " " + ppiRec.LastName;
            }

            return RedirectToAction("Create", "CallBacks", new { custId = customerId, custName = custFullName });
        }

        private TestDBEntities SaveRecordAsCallBack(int customerId, out PPI callBackRecord)
        {
            var entities = new TestDBEntities();
            callBackRecord = entities.PPIs.FirstOrDefault(x => x.Id == customerId);

            if (callBackRecord != null)
            {
                callBackRecord.ExternalStatus = "callback_TBD";
                callBackRecord.LastUpdatedDate = DateTimeStamp;
                callBackRecord.LastUpdatedUser = (int?)UserId;
            }
            entities.SaveChanges();
            return entities;
        }

        private void AddUserDetailsWhoProcessTheRecord(PPI personRecord, TestDBEntities entities)
        {
            personRecord.LastUpdatedDate = DateTimeStamp;
            personRecord.LastUpdatedUser = (int?) UserId;
            personRecord.ExternalStatus = "Allocated";
            personRecord.ProcessedBy = (string) UserName;
            personRecord.ProcessedDate = DateTimeStamp;

            entities.SaveChanges();
        }

        private static void SaveRejectedRecord(PPI model, PPI recordToBeUpdated, object userName, DateTime dateTimeStamp,
            object userId, TestDBEntities entities)
        {
            recordToBeUpdated.FirstName = model.FirstName;
            recordToBeUpdated.LastName = model.LastName;
            recordToBeUpdated.AccessToAccountNumber = model.AccessToAccountNumber;
            recordToBeUpdated.AccessToDocuments = model.AccessToDocuments;
            recordToBeUpdated.Address = model.Address;
            recordToBeUpdated.Amount = model.Amount;
            recordToBeUpdated.Arrears = model.Arrears;
            recordToBeUpdated.ClaimedPPIBefore = model.ClaimedPPIBefore;
            recordToBeUpdated.ContactLender = model.ContactLender;
            recordToBeUpdated.CustomerId = model.CustomerId;
            recordToBeUpdated.Dob = model.Dob;
            recordToBeUpdated.Email = model.Email;
            recordToBeUpdated.EndDate = model.EndDate;
            recordToBeUpdated.FinanceType = model.FinanceType;
            recordToBeUpdated.HasPPI = model.HasPPI;
            recordToBeUpdated.HomePhone = model.HomePhone;
            recordToBeUpdated.Lender = model.Lender;
            recordToBeUpdated.MobilePhone = model.MobilePhone;
            recordToBeUpdated.Postcode = model.Postcode;
            recordToBeUpdated.StartDate = model.StartDate;
            recordToBeUpdated.SubmitToCompanyId = model.SubmitToCompanyId;
            recordToBeUpdated.Title = model.Title;
            recordToBeUpdated.WorkPhone = model.WorkPhone;
            recordToBeUpdated.ExternalNotes = model.ExternalNotes;

            recordToBeUpdated.ExternalStatus = "Rejected";
            recordToBeUpdated.ExternalRejectReason = model.ExternalRejectReason;
            recordToBeUpdated.ProcessedBy = (string) userName;
            recordToBeUpdated.ProcessedDate = dateTimeStamp;


            recordToBeUpdated.LastUpdatedDate = dateTimeStamp;
            recordToBeUpdated.LastUpdatedUser = (int?) userId;

            //No allocated details; Allocation happened only when the record is submitted to process. Also no unique reference

            entities.SaveChanges();
        }

        private static void SaveAcceptedRecord(PPI model, PPI recordToBeUpdated, DateTime dateTimeStamp, object userName,
            TestDBEntities entities)
        {
            recordToBeUpdated.FirstName = model.FirstName;
            recordToBeUpdated.LastName = model.LastName;
            recordToBeUpdated.AccessToAccountNumber = model.AccessToAccountNumber;
            recordToBeUpdated.AccessToDocuments = model.AccessToDocuments;
            recordToBeUpdated.Address = model.Address;
            recordToBeUpdated.Amount = model.Amount;
            recordToBeUpdated.Arrears = model.Arrears;
            recordToBeUpdated.ClaimedPPIBefore = model.ClaimedPPIBefore;
            recordToBeUpdated.ContactLender = model.ContactLender;
            recordToBeUpdated.CustomerId = model.CustomerId;
            recordToBeUpdated.Dob = model.Dob;
            recordToBeUpdated.Email = model.Email;
            recordToBeUpdated.EndDate = model.EndDate;
            recordToBeUpdated.FinanceType = model.FinanceType;
            recordToBeUpdated.HasPPI = model.HasPPI;
            recordToBeUpdated.HomePhone = model.HomePhone;
            recordToBeUpdated.Lender = model.Lender;
            recordToBeUpdated.MobilePhone = model.MobilePhone;
            recordToBeUpdated.Postcode = model.Postcode;
            recordToBeUpdated.StartDate = model.StartDate;
            recordToBeUpdated.SubmitToCompanyId = model.SubmitToCompanyId;
            recordToBeUpdated.Title = model.Title;
            recordToBeUpdated.WorkPhone = model.WorkPhone;
            recordToBeUpdated.ExternalNotes = model.ExternalNotes;

            recordToBeUpdated.LastUpdatedDate = dateTimeStamp;
            recordToBeUpdated.ExternalStatus = "Completed";
            recordToBeUpdated.ProcessedBy = (string) userName;
            recordToBeUpdated.ProcessedDate = dateTimeStamp;

            entities.SaveChanges();
        }
    }
}