﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using DRM.Webportal.DAL;
using DRM.Webportal.Models;

namespace DRM.Webportal.Controllers
{
    public class PpiController : Controller
    {
        public DateTime DateTimeStamp;
        public object UserAccess;
        public object UserId;
        public object UserName;

        public PpiController()
        {
            UserName = System.Web.HttpContext.Current.Session["LoggedInUserName"];
            UserId = System.Web.HttpContext.Current.Session["LoggedInUserId"];
            UserAccess = System.Web.HttpContext.Current.Session["LoggedInUserAccess"];
            DateTimeStamp = DateTime.Now;
        }

       public ActionResult Index(PPI ppi)
        {
            try
            {
                if ((string) UserAccess == "admin" || (string) UserAccess == "user")
                {
                    return View(ppi);
                }
                TempData["notice"] =
                    "You do not have permission to access this page. Please Login using correct credentials !";

                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
                return RedirectToAction("Index", "Ppi");
            }
        }

        [HttpPost]
        public RedirectToRouteResult Loadrecord(PPI model)
        {
            try
            {
                var entities = new TestDBEntities();

                if (Request.Form["btnGetNextRecord"] != null)
                {
                    // get random record where allocated != yes
                    PPI randomPpiRecord =
                        entities.PPIs.OrderBy(x => Guid.NewGuid()).FirstOrDefault(x => x.Allocated != "yes");

                    // make allocated = yes
                    if (randomPpiRecord != null)
                    {
                        randomPpiRecord.Allocated = AllocatedStatus.yes.ToString();
                        entities.SaveChanges();

                        Session["NextRecordButtonStatus"] = "off";
                    }
                    else
                    {
                        Session["NextRecordButtonStatus"] = "on";
                        TempData["notice"] = "No Records Available !!!";
                    }


                    return RedirectToAction("Index", "Ppi", randomPpiRecord);
                }

                if (Request.Form["btnSkip"] != null)
                {
                    // skipping the record

                    PPI recordToBeDeleted = entities.PPIs.FirstOrDefault(x => x.Id == model.Id);
                    PPI recordToBeInserted = entities.PPIs.FirstOrDefault(x => x.Id == model.Id);

                    //delete this record
                    if (recordToBeDeleted != null)
                    {
                        entities.PPIs.Remove(recordToBeDeleted);
                        entities.SaveChanges();
                    }

                    //insert this record again as a new one
                    if (recordToBeInserted != null)
                    {
                        InsertTheSkippedRecordAsNewOne(model, recordToBeInserted, entities);
                    }

                    Session["NextRecordButtonStatus"] = "on";

                    return RedirectToAction("Index", "Ppi");
                }

                if (Request.Form["submitButton"] != null)
                {
                    // submit to process
                    PPI recordToBeUpdated = entities.PPIs.FirstOrDefault(x => x.Id == model.Id);
                    if (recordToBeUpdated != null)
                    {
                        UpdateSubmittedRecord(model, recordToBeUpdated, entities);
                    }
                    TempData["notice"] = "Customer Details Submitted to Process !";

                    Session["NextRecordButtonStatus"] = "on";

                    return RedirectToAction("Index", "ViewPpi", new {ppiId = model.Id});
                }

                if (Request.Form["rejectbutton"] != null)
                {
                    // Reject the record and save.
                    PPI recordToBeUpdated = entities.PPIs.FirstOrDefault(x => x.Id == model.Id);

                    if (recordToBeUpdated != null)
                    {
                        UpdateRejectedRecord(model, recordToBeUpdated, entities);
                    }

                    TempData["notice"] = "Customer Details Updated !";

                    Session["NextRecordButtonStatus"] = "on";

                    return RedirectToAction("Index", "Ppi");
                }
                return RedirectToAction("Index", "Ppi");
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;

                return RedirectToAction("Index", "Ppi");
            }
        }

        private void UpdateRejectedRecord(PPI model, PPI recordToBeUpdated, TestDBEntities entities)
        {
            recordToBeUpdated.FirstName = model.FirstName;
            recordToBeUpdated.LastName = model.LastName;
            recordToBeUpdated.AccessToAccountNumber = model.AccessToAccountNumber;
            recordToBeUpdated.AccessToDocuments = model.AccessToDocuments;
            recordToBeUpdated.Address = model.Address;
            recordToBeUpdated.Amount = model.Amount;
            recordToBeUpdated.Arrears = model.Arrears;
            recordToBeUpdated.ClaimedPPIBefore = model.ClaimedPPIBefore;
            recordToBeUpdated.ContactLender = model.ContactLender;
            recordToBeUpdated.CustomerId = model.CustomerId;
            recordToBeUpdated.Dob = model.Dob;
            recordToBeUpdated.Email = model.Email;
            recordToBeUpdated.EndDate = model.EndDate;
            recordToBeUpdated.FinanceType = model.FinanceType;
            recordToBeUpdated.HasPPI = model.HasPPI;
            recordToBeUpdated.HomePhone = model.HomePhone;
            recordToBeUpdated.Lender = model.Lender;
            recordToBeUpdated.MobilePhone = model.MobilePhone;
            recordToBeUpdated.Postcode = model.Postcode;
            recordToBeUpdated.StartDate = model.StartDate;
            recordToBeUpdated.SubmitToCompanyId = model.SubmitToCompanyId;
            recordToBeUpdated.Title = model.Title;
            recordToBeUpdated.WorkPhone = model.WorkPhone;
            recordToBeUpdated.AgentNotes = model.AgentNotes;
            recordToBeUpdated.ExternalNotes = model.ExternalNotes;

            recordToBeUpdated.AgentStatus = "Rejected";
            recordToBeUpdated.AgentRejectReason = model.AgentRejectReason;

            recordToBeUpdated.LastUpdatedDate = DateTimeStamp;
            recordToBeUpdated.LastUpdatedUser = (int?) UserId;


            entities.SaveChanges();
        }

        private void UpdateSubmittedRecord(PPI model, PPI recordToBeUpdated, TestDBEntities entities)
        {
            recordToBeUpdated.FirstName = model.FirstName;
            recordToBeUpdated.LastName = model.LastName;
            recordToBeUpdated.AccessToAccountNumber = model.AccessToAccountNumber;
            recordToBeUpdated.AccessToDocuments = model.AccessToDocuments;
            recordToBeUpdated.Address = model.Address;
            recordToBeUpdated.Amount = model.Amount;
            recordToBeUpdated.Arrears = model.Arrears;
            recordToBeUpdated.ClaimedPPIBefore = model.ClaimedPPIBefore;
            recordToBeUpdated.ContactLender = model.ContactLender;
            recordToBeUpdated.CustomerId = model.CustomerId;
            recordToBeUpdated.Dob = model.Dob;
            recordToBeUpdated.Email = model.Email;
            recordToBeUpdated.EndDate = model.EndDate;
            recordToBeUpdated.FinanceType = model.FinanceType;
            recordToBeUpdated.HasPPI = model.HasPPI;
            recordToBeUpdated.HomePhone = model.HomePhone;
            recordToBeUpdated.Lender = model.Lender;
            recordToBeUpdated.MobilePhone = model.MobilePhone;
            recordToBeUpdated.Postcode = model.Postcode;
            recordToBeUpdated.StartDate = model.StartDate;
            recordToBeUpdated.SubmitToCompanyId = model.SubmitToCompanyId;
            recordToBeUpdated.Title = model.Title;
            recordToBeUpdated.WorkPhone = model.WorkPhone;
            recordToBeUpdated.AgentNotes = model.AgentNotes;
            recordToBeUpdated.ExternalNotes = model.ExternalNotes;
            recordToBeUpdated.AgentRejectReason = model.AgentRejectReason;


            recordToBeUpdated.Reference = GenerateUniqueReference();
            recordToBeUpdated.AgentStatus = "Submitted";

            recordToBeUpdated.LastUpdatedDate = DateTimeStamp;
            recordToBeUpdated.LastUpdatedUser = (int?) UserId;

            recordToBeUpdated.AllocatedDate = DateTimeStamp; // this is only when submitted
            recordToBeUpdated.AllocatedTo = (string) UserName; // this is only when submitted


            entities.SaveChanges();
        }

        private void InsertTheSkippedRecordAsNewOne(PPI model, PPI recordToBeInserted, TestDBEntities entities)
        {
            recordToBeInserted.FirstName = model.FirstName;
            recordToBeInserted.LastName = model.LastName;
            recordToBeInserted.AccessToAccountNumber = model.AccessToAccountNumber;
            recordToBeInserted.AccessToDocuments = model.AccessToDocuments;
            recordToBeInserted.Address = model.Address;
            recordToBeInserted.Amount = model.Amount;
            recordToBeInserted.Arrears = model.Arrears;
            recordToBeInserted.ClaimedPPIBefore = model.ClaimedPPIBefore;
            recordToBeInserted.ContactLender = model.ContactLender;
            recordToBeInserted.CustomerId = model.CustomerId;
            recordToBeInserted.Dob = model.Dob;
            recordToBeInserted.Email = model.Email;
            recordToBeInserted.EndDate = model.EndDate;
            recordToBeInserted.FinanceType = model.FinanceType;
            recordToBeInserted.HasPPI = model.HasPPI;
            recordToBeInserted.HomePhone = model.HomePhone;
            recordToBeInserted.Lender = model.Lender;
            recordToBeInserted.MobilePhone = model.MobilePhone;
            recordToBeInserted.Postcode = model.Postcode;
            recordToBeInserted.StartDate = model.StartDate;
            //recordToBeInserted.SubmitToCompanyId = model.SubmitToCompanyId;
            recordToBeInserted.Title = model.Title;
            recordToBeInserted.WorkPhone = model.WorkPhone;
            recordToBeInserted.AgentNotes = model.AgentNotes;
            recordToBeInserted.ExternalNotes = model.ExternalNotes;
            //recordToBeInserted.AgentRejectReason = model.AgentRejectReason;

            recordToBeInserted.AgentStatus = "skipped";
            recordToBeInserted.Allocated = "";

            recordToBeInserted.LastUpdatedDate = DateTimeStamp;
            recordToBeInserted.LastUpdatedUser = (int?) UserId;

            entities.PPIs.Add(recordToBeInserted);
            entities.SaveChanges();
        }

        public string GenerateUniqueReference()
        {
            //alpha numeric
            //string def = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            //var rnd = new Random();
            //var ret = new StringBuilder();
            //for (int i = 0; i < 6; i++)
            //    ret.Append(def.Substring(rnd.Next(def.Length), 1));
            //return ret.ToString();

            // numeric
            var rnd = new Random();
            int myRandomNo = rnd.Next(0000001, 99999999);

            return myRandomNo.ToString(CultureInfo.InvariantCulture);
        }

        private enum AllocatedStatus
        {
            yes,
            no
        }

        public ActionResult CallBack(int customerId)
        {
            Session["NextRecordButtonStatus"] = "on";

            // set record as a callback
            PPI callBackRecord;
            var entities = SaveRecordAsCallBack(customerId, out callBackRecord);

            var ppiRec = entities.PPIs.FirstOrDefault(x => x.Id == customerId);
            string custFullName = null;
            if (ppiRec != null)
            {
                 custFullName = ppiRec.FirstName + " " + ppiRec.LastName;
            }

            return RedirectToAction("Create", "CallBacks", new { custId = customerId, custName = custFullName });
        }

        private TestDBEntities SaveRecordAsCallBack(int customerId, out PPI callBackRecord)
        {
            var entities = new TestDBEntities();
            callBackRecord = entities.PPIs.FirstOrDefault(x => x.Id == customerId);

            if (callBackRecord != null)
            {
                callBackRecord.AgentStatus = "callback_TBD";
                callBackRecord.Allocated = "";
                callBackRecord.LastUpdatedDate = DateTimeStamp;
                callBackRecord.LastUpdatedUser = (int?) UserId;
            }
            entities.SaveChanges();
            return entities;
        }
    }
}