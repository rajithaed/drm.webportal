﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using DRM.Webportal.DAL;
using DRM.Webportal.Models;

namespace DRM.Webportal.Controllers
{
    public class DashboardController : Controller
    {
        public object UserAccess;
        private string _selectedDate;

        public DashboardController()
        {
            UserAccess = System.Web.HttpContext.Current.Session["LoggedInUserAccess"];
        }


        public ActionResult Index(DashboardDailySummaryModel model)
        {
            try
            {
                if (model == null) throw new ArgumentNullException("model");

                if ((string) UserAccess == "admin")
                {
                    _selectedDate = model.DateSelected.ToShortDateString();

                    if (string.IsNullOrEmpty(_selectedDate) || _selectedDate == "01/01/0001")
                    {
                        _selectedDate = DateTime.Now.ToShortDateString();
                    }

                    List<PPI> ppiList;

                    using (var entities = new TestDBEntities())
                    {
                        DateTime date = Convert.ToDateTime(_selectedDate);
                        ppiList =
                            new List<PPI>(
                                entities.PPIs.Where(
                                    x =>
                                        x.AllocatedDate.Value.Year == date.Year &&
                                        x.AllocatedDate.Value.Month == date.Month &&
                                        x.AllocatedDate.Value.Day == date.Day));
                    }

                    model = new DashboardDailySummaryModel
                    {
                        TotalRecords = ppiList.Count(),
                        Accepted = ppiList.Count(x => x.ExternalStatus == "Completed"),
                        Rejected = ppiList.Count(x => x.ExternalStatus == "Rejected"),
                        Pending = ppiList.Count(x => x.ExternalStatus == ""),
                        DateSelected = Convert.ToDateTime(_selectedDate),
                    };

                    return View(model);
                }
                TempData["notice"] =
                    "You do not have permission to access this page. Please Login using correct credentials !";

                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
                return View();
            }
        }


        [HttpPost]
        public ActionResult Update(string selectedDate)
        {
            try
            {
                List<PPI> ppiList;
                using (var entities = new TestDBEntities())
                {
                    DateTime date = Convert.ToDateTime(selectedDate);
                    ppiList =
                        new List<PPI>(
                            entities.PPIs.Where(
                                x =>
                                    x.AllocatedDate.Value.Year == date.Year && x.AllocatedDate.Value.Month == date.Month &&
                                    x.AllocatedDate.Value.Day == date.Day));
                }

                var model = new DashboardDailySummaryModel
                {
                    TotalRecords = ppiList.Count(),
                    Accepted = ppiList.Count(x => x.ExternalStatus == "Completed"),
                    Rejected = ppiList.Count(x => x.ExternalStatus == "Rejected"),
                    Pending = ppiList.Count(x => x.ExternalStatus == ""),
                    DateSelected = Convert.ToDateTime(selectedDate)
                };


                return RedirectToAction("Index", "Dashboard", model);
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
                return RedirectToAction("Index", "Dashboard");
            }
        }

        [HttpGet]
        public ActionResult GetMonthlyData(int year, int month)
        {
            //temp
            //int year = 2015;
            //int month = 1;

            var monthlyDataList = new List<MonthlyDataModel>();
            var entities = new TestDBEntities();

            for (int i = 1; i <= DateTime.DaysInMonth(year, month); i++)
            {
                var dateLookinFor = new DateTime(year, month, i);
                PPI[] ppiListForTheDay =
                    new List<PPI>(
                        entities.PPIs.Where(
                            x =>
                                x.AllocatedDate.Value.Year == dateLookinFor.Year &&
                                x.AllocatedDate.Value.Month == dateLookinFor.Month &&
                                x.AllocatedDate.Value.Day == dateLookinFor.Day)).ToArray();

                int acceptsForTheDay = ppiListForTheDay.Count(x => x.ExternalStatus == "Completed");
                int rejectsForTheDay = ppiListForTheDay.Count(x => x.ExternalStatus == "Rejected");
                int skipsForTheDay = ppiListForTheDay.Count(x => string.IsNullOrEmpty(x.ExternalStatus));

                // arrange object
                var model = new MonthlyDataModel
                {
                    Accepted = acceptsForTheDay.ToString(CultureInfo.InvariantCulture),
                    Rejected = rejectsForTheDay.ToString(CultureInfo.InvariantCulture),
                    Skipped = skipsForTheDay.ToString(CultureInfo.InvariantCulture),
                    DateTime = dateLookinFor.ToShortDateString()
                };

                monthlyDataList.Add(model);
            }

            var jsonSerialiser = new JavaScriptSerializer();
            string json = jsonSerialiser.Serialize(monthlyDataList);

            return Json(monthlyDataList, JsonRequestBehavior.AllowGet);
        }
    }

    public class MonthlyDataModel
    {
        public string Accepted { get; set; }
        public string Rejected { get; set; }
        public string Skipped { get; set; }
        public string DateTime { get; set; }
    }
}