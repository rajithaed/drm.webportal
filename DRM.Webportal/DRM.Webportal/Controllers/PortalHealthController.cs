﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DRM.Webportal.DAL;
using DRM.Webportal.Models;

namespace DRM.Webportal.Controllers
{
    public class PortalHealthController : Controller
    {
        public object UserAccess;

        public PortalHealthController()
        {
            UserAccess = System.Web.HttpContext.Current.Session["LoggedInUserAccess"];
        }

        public ActionResult Index(PortalHealthModel model)
        {
            if (model == null) throw new ArgumentNullException("model");
            try
            {
                if ((string) UserAccess == "admin")
                {
                    List<PPI> dbRecordsList;
                    using (var entities = new TestDBEntities())
                    {
                        dbRecordsList = new List<PPI>(entities.PPIs);
                    }

                    model = new PortalHealthModel
                    {
                        TotalDatabaseRecords = dbRecordsList.Count,
                        FreshDbRecords = dbRecordsList.Count(x => string.IsNullOrEmpty(x.Allocated)),
                        UsedRecords = dbRecordsList.Count(x => x.Allocated == "yes"),
                        SkippedRecords = dbRecordsList.Count(x => x.AgentStatus == "skipped")
                    };

                    return View(model);
                }
                TempData["notice"] =
                    "You do not have permission to access this page. Please Login using correct credentials !";

                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                TempData["notice"] = "Error : " + ex.Message;
                return View();
            }
        }
    }
}