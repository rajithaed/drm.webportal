﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DRM.Webportal.Models
{
    public class PortalHealthModel
    {
        public int TotalDatabaseRecords { get; set; }
        public int FreshDbRecords { get; set; }
        public int SkippedRecords { get; set; }
        public int UsedRecords { get; set; }
    }
}