﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace DRM.Webportal.Models
{
    public class DashboardDailySummaryModel
    {
        public int TotalRecords { get; set; }
        public int Accepted { get; set; }
        public int Rejected { get; set; }
        public int Pending { get; set; }
        public DateTime DateSelected { get; set; }
        

    }
}